﻿using System;
using System.Collections.Generic;
using _3LayerArchitecture.DataAccess.Dto;

namespace _3LayerArchitecture.DataAccess.FileSystem
{
    public class FileSystemDebtDAO : IDebtDAO
    {
        public readonly FileSystemConfiguration _configuration;

        public FileSystemDebtDAO(FileSystemConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Save(DebtDto debt)
        {
            // загружаем файл
            var file = StorageFileUtils.LoadFile(_configuration.PathToFile);

            file.Debts.Add(debt);

            // пересохраняем файл
            StorageFileUtils.SaveFile(_configuration.PathToFile, file);
        }

        public void Update(DebtDto debt)
        {
            // загружаем файл
            var file = StorageFileUtils.LoadFile(_configuration.PathToFile);

            // находим индекс элемента, который нужно заменять и собственно заменяем
            var indexToUpdate = file.Debts.FindIndex(x => x.Id == debt.Id);
            file.Debts[indexToUpdate] = debt;

            // пересохраняем файл
            StorageFileUtils.SaveFile(_configuration.PathToFile, file);
        }

        public DebtDto FindById(Guid id)
        {
            // загружаем файл
            var file = StorageFileUtils.LoadFile(_configuration.PathToFile);

            // ищем элемент по id
            return file.Debts.Find(x => x.Id == id);
        }

        public IEnumerable<DebtDto> GetAll()
        {
            // загружаем файл
            var file = StorageFileUtils.LoadFile(_configuration.PathToFile);

            return file.Debts;
        }
    }
}