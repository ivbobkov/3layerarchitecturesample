﻿using System.IO;
using Newtonsoft.Json;

namespace _3LayerArchitecture.DataAccess.FileSystem
{
    public static class StorageFileUtils
    {
        public static StorageFile LoadFile(string pathToFile)
        {
            if (File.Exists(pathToFile))
            {
                var rawContent = File.ReadAllText(pathToFile);

                return JsonConvert.DeserializeObject<StorageFile>(rawContent);
            }

            return new StorageFile();
        }

        public static void SaveFile(string pathToFile, StorageFile storageFile)
        {
            File.WriteAllText(pathToFile, JsonConvert.SerializeObject(storageFile, Formatting.Indented));
        }
    }
}