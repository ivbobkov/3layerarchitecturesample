﻿using System;
using System.Collections.Generic;
using _3LayerArchitecture.DataAccess.Dto;

namespace _3LayerArchitecture.DataAccess.FileSystem
{
    public class FileSystemLoanDAO : ILoanDAO
    {
        public readonly FileSystemConfiguration _configuration;

        public FileSystemLoanDAO(FileSystemConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Save(LoanDto loan)
        {
            // загружаем файл
            var file = StorageFileUtils.LoadFile(_configuration.PathToFile);

            file.Loans.Add(loan);

            // пересохраняем файл
            StorageFileUtils.SaveFile(_configuration.PathToFile, file);
        }

        public void Update(LoanDto loan)
        {
            // загружаем файл
            var file = StorageFileUtils.LoadFile(_configuration.PathToFile);

            // находим индекс элемента, который нужно заменять и собственно заменяем
            var indexToUpdate = file.Loans.FindIndex(x => x.Id == loan.Id);
            file.Loans[indexToUpdate] = loan;

            // пересохраняем файл
            StorageFileUtils.SaveFile(_configuration.PathToFile, file);
        }

        public LoanDto FindById(Guid id)
        {
            // загружаем файл
            var file = StorageFileUtils.LoadFile(_configuration.PathToFile);

            // ищем элемент по id
            return file.Loans.Find(x => x.Id == id);
        }

        public IEnumerable<LoanDto> GetAll()
        {
            // загружаем файл
            var file = StorageFileUtils.LoadFile(_configuration.PathToFile);

            return file.Loans;
        }
    }
}