﻿namespace _3LayerArchitecture.DataAccess.FileSystem
{
    public class FileSystemConfiguration
    {
        public FileSystemConfiguration(string pathToFile)
        {
            PathToFile = pathToFile;
        }

        public string PathToFile { get; set; }
    }
}