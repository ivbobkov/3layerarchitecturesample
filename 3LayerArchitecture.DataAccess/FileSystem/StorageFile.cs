﻿using System.Collections.Generic;
using _3LayerArchitecture.DataAccess.Dto;

namespace _3LayerArchitecture.DataAccess.FileSystem
{
    public class StorageFile
    {
        public List<DebtDto> Debts { get; set; } = new List<DebtDto>();
        public List<LoanDto> Loans { get; set; } = new List<LoanDto>();
    }
}