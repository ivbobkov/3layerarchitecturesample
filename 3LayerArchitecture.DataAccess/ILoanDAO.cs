﻿using System;
using System.Collections.Generic;
using _3LayerArchitecture.DataAccess.Dto;

namespace _3LayerArchitecture.DataAccess
{
    public interface ILoanDAO
    {
        void Save(LoanDto loan);
        void Update(LoanDto loan);
        LoanDto FindById(Guid id);
        IEnumerable<LoanDto> GetAll();
    }
}