﻿using System;

namespace _3LayerArchitecture.DataAccess.Dto
{
    public class DebtDto
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public string CreditorName { get; set; }
        public int Status { get; set; }
    }
}