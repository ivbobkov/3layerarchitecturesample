﻿using System;

namespace _3LayerArchitecture.DataAccess.Dto
{
    public class LoanDto
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public string BorrowerName { get; set; }
        public int Status { get; set; }
    }
}