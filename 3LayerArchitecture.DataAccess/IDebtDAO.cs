﻿using System;
using System.Collections.Generic;
using _3LayerArchitecture.DataAccess.Dto;

namespace _3LayerArchitecture.DataAccess
{
    public interface IDebtDAO
    {
        void Save(DebtDto debt);
        void Update(DebtDto debt);
        DebtDto FindById(Guid id);
        IEnumerable<DebtDto> GetAll();
    }
}