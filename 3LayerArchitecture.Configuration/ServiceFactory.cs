﻿using _3LayerArchitecture.DataAccess;
using _3LayerArchitecture.Domain.Services;

namespace _3LayerArchitecture.Configuration
{
    public static class ServiceFactory
    {
        public static IDebtService CreateDebtService(IDebtDAO debtDAO)
        {
            return new DebtService(debtDAO);
        }

        public static ILoanService CreateLoanService(ILoanDAO loanDAO)
        {
            return new LoanService(loanDAO);
        }
    }
}