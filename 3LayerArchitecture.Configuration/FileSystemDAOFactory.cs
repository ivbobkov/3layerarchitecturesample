﻿using _3LayerArchitecture.DataAccess;
using _3LayerArchitecture.DataAccess.FileSystem;

namespace _3LayerArchitecture.Configuration
{
    public static class FileSystemDAOFactory
    {
        public static IDebtDAO CreateDebtDAO(string pathToFile)
        {
            return new FileSystemDebtDAO(new FileSystemConfiguration(pathToFile));
        }

        public static ILoanDAO CreateLoanDAO(string pathToFile)
        {
            return new FileSystemLoanDAO(new FileSystemConfiguration(pathToFile));
        }
    }
}