﻿using System;
using _3LayerArchitecture.DataAccess;

namespace _3LayerArchitecture.Configuration
{
    // Если вдруг, мы захотим использовать MySQL, то нам понадобится:
    // 1) Подключить библиотеку для работы с MySQL
    // 2) Реализовать интерфейсы IDebtDAO и ILoanDAO и положить релизации в папку MySql(ну и создать) в проекте 3LayerArchitecture.DataAccess
    // 3) Использовать MySqlDAOFactory вместо FileSystemDAOFactory
    public static class MySqlDAOFactory
    {
        public static IDebtDAO CreateDebtDAO(string connectionString)
        {
            throw new NotImplementedException();
        }

        public static ILoanDAO CreateLoanDAO(string connectionString)
        {
            throw new NotImplementedException();
        }
    }
}