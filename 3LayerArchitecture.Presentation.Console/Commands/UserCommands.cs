﻿namespace _3LayerArchitecture.Presentation.Console.Commands
{
    public static class UserCommands
    {
        public const string ListLoans = "список заемов";
        public const string ListDebts = "список долгов";
        public const string CreateLoan = "создать заем";
        public const string CreateDebt = "создать долг";
        public const string CloseLoan = "закрыть заем";
        public const string ReturnDebt = "вернуть долг";
        public const string Help = "помощь";
        public const string Exit = "выход";
    }
}