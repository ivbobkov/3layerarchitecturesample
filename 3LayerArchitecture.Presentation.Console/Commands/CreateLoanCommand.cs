﻿using _3LayerArchitecture.Domain.Services;

namespace _3LayerArchitecture.Presentation.Console.Commands
{
    public class CreateLoanCommand : ICommand
    {
        private readonly ILoanService _loanService;

        public CreateLoanCommand(ILoanService loanService)
        {
            _loanService = loanService;
        }

        public void Execute()
        {
            System.Console.Write("Описание > ");
            var description = System.Console.ReadLine();

            System.Console.Write("Сумма > ");
            var rawAmount = System.Console.ReadLine();

            if (!decimal.TryParse(rawAmount, out var amount))
            {
                System.Console.WriteLine("Неверно введенно число");
                return;
            }

            System.Console.Write("Заёмщик > ");
            var creditorName = System.Console.ReadLine();

            _loanService.Create(description, amount, creditorName);
            System.Console.WriteLine("Заём создан");
        }
    }
}