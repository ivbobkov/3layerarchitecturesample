﻿using System;
using System.Runtime.InteropServices;
using _3LayerArchitecture.Domain.Services;

namespace _3LayerArchitecture.Presentation.Console.Commands
{
    public class CloseLoanCommand : ICommand
    {
        private readonly ILoanService _loanService;

        public CloseLoanCommand(ILoanService loanService)
        {
            _loanService = loanService;
        }

        public void Execute()
        {
            System.Console.Write("Введите ID заема > ");
            var userInput = System.Console.ReadLine();

            if (Guid.TryParse(userInput, out var id))
            {
                _loanService.Close(id);
                System.Console.WriteLine("Заём закрыт");
            }
            else
            {
                System.Console.WriteLine("Введен некорректный ID");
            }
        }
    }
}