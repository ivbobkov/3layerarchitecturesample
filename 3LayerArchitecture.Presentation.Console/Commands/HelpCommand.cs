﻿namespace _3LayerArchitecture.Presentation.Console.Commands
{
    public class HelpCommand : ICommand
    {
        public void Execute()
        {
            System.Console.WriteLine("Список доступных комманд");
            System.Console.WriteLine(UserCommands.ListLoans);
            System.Console.WriteLine(UserCommands.ListDebts);
            System.Console.WriteLine(UserCommands.CreateLoan);
            System.Console.WriteLine(UserCommands.CreateDebt);
            System.Console.WriteLine(UserCommands.CloseLoan);
            System.Console.WriteLine(UserCommands.ReturnDebt);
            System.Console.WriteLine(UserCommands.Help);
            System.Console.WriteLine(UserCommands.Exit);
        }
    }
}