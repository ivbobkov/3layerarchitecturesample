﻿namespace _3LayerArchitecture.Presentation.Console.Commands
{
    public interface ICommand
    {
        void Execute();
    }
}