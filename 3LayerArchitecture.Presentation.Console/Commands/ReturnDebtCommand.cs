﻿using System;
using _3LayerArchitecture.Domain.Services;

namespace _3LayerArchitecture.Presentation.Console.Commands
{
    public class ReturnDebtCommand : ICommand
    {
        private readonly IDebtService _debtService;

        public ReturnDebtCommand(IDebtService debtService)
        {
            _debtService = debtService;
        }

        public void Execute()
        {
            System.Console.Write("Введите ID долга > ");
            var userInput = System.Console.ReadLine();

            if (Guid.TryParse(userInput, out var id))
            {
                _debtService.Return(id);
                System.Console.WriteLine("Долг возвращён");
            }
            else
            {
                System.Console.WriteLine("Введен некорректный ID");
            }
        }
    }
}