﻿using _3LayerArchitecture.Domain.Services;

namespace _3LayerArchitecture.Presentation.Console.Commands
{
    public class CreateDebtCommand : ICommand
    {
        private readonly IDebtService _debtService;

        public CreateDebtCommand(IDebtService debtService)
        {
            _debtService = debtService;
        }

        public void Execute()
        {
            System.Console.Write("Описание > ");
            var description = System.Console.ReadLine();

            System.Console.Write("Сумма > ");
            var rawAmount = System.Console.ReadLine();

            if (!decimal.TryParse(rawAmount, out var amount))
            {
                System.Console.WriteLine("Неверно введенно число");
                return;
            }

            System.Console.Write("Кредитор > ");
            var creditorName = System.Console.ReadLine();

            _debtService.Create(description, amount, creditorName);
            System.Console.WriteLine("Долг создан");
        }
    }
}