﻿using System.Linq;
using _3LayerArchitecture.Domain.Services;

namespace _3LayerArchitecture.Presentation.Console.Commands
{
    public class ListLoansCommand : ICommand
    {
        private readonly ILoanService _loanService;

        public ListLoansCommand(ILoanService loanService)
        {
            _loanService = loanService;
        }

        public void Execute()
        {
            var debts = _loanService.GetAll().ToList();

            for (int index = 0; index < debts.Count; index++)
            {
                var debt = debts[index];

                PrintNameValue("ID", debt.Id.ToString());
                PrintNameValue("Описание", debt.Description);
                PrintNameValue("Сумма", debt.Amount.ToString("F1"));
                PrintNameValue("Заёмщик", debt.BorrowerName);
                PrintNameValue("Статус", debt.Status.ToString());

                if (index != debts.Count - 1)
                {
                    System.Console.WriteLine();
                }
            }
        }

        private void PrintNameValue(string key, string value)
        {
            System.Console.WriteLine($"{key}: {value}");
        }
    }
}