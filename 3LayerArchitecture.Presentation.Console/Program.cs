﻿using _3LayerArchitecture.Configuration;

namespace _3LayerArchitecture.Presentation.Console
{
    internal class Program
    {
        // можно хоть брать из параметров запуска программы, по идее просто пробросить в метод Main() вот это - string[] args и тянуть оттуда.
        private const string PathToFile = @"D:\Projects\Mentor\db.json";

        private static void Main()
        {
            var loanService = ServiceFactory.CreateLoanService(FileSystemDAOFactory.CreateLoanDAO(PathToFile));
            var debtService = ServiceFactory.CreateDebtService(FileSystemDAOFactory.CreateDebtDAO(PathToFile));

            var application = new Application(loanService, debtService);
            application.Run();
        }
    }
}
