﻿using System;
using _3LayerArchitecture.Domain.Services;
using _3LayerArchitecture.Presentation.Console.Commands;

namespace _3LayerArchitecture.Presentation.Console
{
    public class Application
    {
        private readonly ILoanService _loanService;
        private readonly IDebtService _debtService;

        public Application(ILoanService loanService, IDebtService debtService)
        {
            _loanService = loanService;
            _debtService = debtService;
        }

        public void Run()
        {
            string userInput = string.Empty;
            ICommand actualCommand = null;
            PrintGreeting();

            while (true)
            {
                System.Console.Write("Введите комманду > ");
                userInput = System.Console.ReadLine();

                if (ShouldExit(userInput))
                {
                    break;
                }

                try
                {
                    System.Console.WriteLine();
                    actualCommand = RecognizeCommandByUserInput(userInput);
                    actualCommand.Execute();
                    System.Console.WriteLine();
                }
                catch (ArgumentException ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        private static void PrintGreeting()
        {
            System.Console.WriteLine($"Для вывода списка доступных комманд введите: {UserCommands.Help}");
        }

        private static bool ShouldExit(string userInput)
        {
            return !string.IsNullOrWhiteSpace(userInput) && userInput.Equals(UserCommands.Exit);
        }

        private ICommand RecognizeCommandByUserInput(string userInput)
        {
            switch (userInput)
            {
                case UserCommands.ListLoans: return new ListLoansCommand(_loanService);
                case UserCommands.ListDebts: return new ListDebtsCommand(_debtService);
                case UserCommands.CreateLoan: return new CreateLoanCommand(_loanService);
                case UserCommands.CreateDebt: return new CreateDebtCommand(_debtService);
                case UserCommands.CloseLoan: return new CloseLoanCommand(_loanService);
                case UserCommands.ReturnDebt: return new ReturnDebtCommand(_debtService);
                case UserCommands.Help: return new HelpCommand();
                default: throw new ArgumentException("Введена неверная команда.");
            }
        }
    }
}