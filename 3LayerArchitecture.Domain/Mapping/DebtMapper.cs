﻿using _3LayerArchitecture.DataAccess.Dto;
using _3LayerArchitecture.Domain.Models;

namespace _3LayerArchitecture.Domain.Mapping
{
    public static class DebtMapper
    {
        public static Debt Map(DebtDto debtDto)
        {
            return new Debt(
                debtDto.Id,
                debtDto.Description,
                debtDto.Amount,
                debtDto.CreditorName,
                (DebtStatus)debtDto.Status);
        }

        public static DebtDto Map(Debt debt)
        {
            return new DebtDto
            {
                Id = debt.Id,
                Amount = debt.Amount,
                CreditorName = debt.CreditorName,
                Description = debt.Description,
                Status = (int)debt.Status
            };
        }
    }
}