﻿using _3LayerArchitecture.DataAccess.Dto;
using _3LayerArchitecture.Domain.Models;

namespace _3LayerArchitecture.Domain.Mapping
{
    public static class LoanMapper
    {
        public static Loan Map(LoanDto debtDto)
        {
            return new Loan(
                debtDto.Id,
                debtDto.Description,
                debtDto.Amount,
                debtDto.BorrowerName,
                (LoanStatus)debtDto.Status);
        }

        public static LoanDto Map(Loan debt)
        {
            return new LoanDto
            {
                Id = debt.Id,
                Amount = debt.Amount,
                BorrowerName = debt.BorrowerName,
                Description = debt.Description,
                Status = (int)debt.Status
            };
        }
    }
}