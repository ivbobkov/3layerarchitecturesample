﻿namespace _3LayerArchitecture.Domain.Models
{
    public enum DebtStatus
    {
        New,
        Returned
    }
}