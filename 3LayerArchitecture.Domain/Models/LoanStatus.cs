﻿namespace _3LayerArchitecture.Domain.Models
{
    public enum LoanStatus
    {
        New,
        Closed
    }
}