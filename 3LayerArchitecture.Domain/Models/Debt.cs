﻿using System;

namespace _3LayerArchitecture.Domain.Models
{
    // Долг - описывает то, кому ты должен
    public class Debt
    {
        public Debt(
            Guid id,
            string description,
            decimal amount,
            string creditorName,
            DebtStatus status = DebtStatus.New)
        {
            Id = id;
            Description = description;
            Amount = amount > 0 ? amount : throw new ArgumentException("Сумма должна быть больше 0");
            CreditorName = creditorName;
            Status = status;
        }

        public Guid Id { get; }
        public string Description { get; private set; }
        public decimal Amount { get; private set; }
        public string CreditorName { get; private set; } // кредитор, тот кто дал денег
        public DebtStatus Status { get; private set; }

        public void Return()
        {
            Status = DebtStatus.Returned;
        }

        public void Update(string description, decimal amount, string creditorName)
        {
            Description = description;
            Amount = amount;
            CreditorName = creditorName;
        }
    }
}