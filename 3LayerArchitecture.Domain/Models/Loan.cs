﻿using System;

namespace _3LayerArchitecture.Domain.Models
{
    // Заем - описывает то кто должен тебе
    public class Loan
    {
        public Loan(Guid id, string description, decimal amount, string borrowerName, LoanStatus status = LoanStatus.New)
        {
            Id = id;
            Description = description;
            Amount = amount > 0 ? amount : throw new ArgumentException("Сумма должна быть больше 0");
            BorrowerName = borrowerName;
            Status = status;
        }

        public Guid Id { get; private set; }
        public string Description { get; private set; }
        public decimal Amount { get; private set; }
        public string BorrowerName { get; private set; }
        public LoanStatus Status { get; private set; }

        public void Update(string description, decimal amount, string borrowerName)
        {
            Description = description;
            Amount = amount;
            BorrowerName = borrowerName;
        }

        public void Close()
        {
            Status = LoanStatus.Closed;
        }
    }
}