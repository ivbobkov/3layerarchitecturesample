﻿using System;
using System.Collections.Generic;
using _3LayerArchitecture.Domain.Models;

namespace _3LayerArchitecture.Domain.Services
{
    public interface ILoanService
    {
        IEnumerable<Loan> GetAll();
        void Create(string description, decimal amount, string borrowerName);
        void Update(Guid id, string description, decimal amount, string borrowerName);
        void Close(Guid id);
    }
}