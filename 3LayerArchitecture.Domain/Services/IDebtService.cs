﻿using System;
using System.Collections.Generic;
using _3LayerArchitecture.Domain.Models;

namespace _3LayerArchitecture.Domain.Services
{
    public interface IDebtService
    {
        IEnumerable<Debt> GetAll();
        void Create(string description, decimal amount, string creditorName);
        void Update(Guid id, string description, decimal amount, string creditorName);
        void Return(Guid id);
    }
}