﻿using System;
using System.Collections.Generic;
using System.Linq;
using _3LayerArchitecture.DataAccess;
using _3LayerArchitecture.Domain.Mapping;
using _3LayerArchitecture.Domain.Models;

namespace _3LayerArchitecture.Domain.Services
{
    public class DebtService : IDebtService
    {
        private readonly IDebtDAO _debtDAO;

        public DebtService(IDebtDAO debtDAO)
        {
            _debtDAO = debtDAO;
        }

        public IEnumerable<Debt> GetAll()
        {
            var debtDtos = _debtDAO.GetAll();

            return debtDtos.Select(debtDto => DebtMapper.Map(debtDto));
        }

        public void Create(string description, decimal amount, string creditorName)
        {
            // создаем бизнес-объект
            var debt = new Debt(Guid.NewGuid(), description, amount, creditorName);

            // приводим бизнес-объект к объекту хранилища и сохраняем
            var debtDto = DebtMapper.Map(debt);
            _debtDAO.Save(debtDto);
        }

        public void Update(Guid id, string description, decimal amount, string creditorName)
        {
            // извлекаем объект хранилища и приводим его к бизнес-объекту
            var debtDto = _debtDAO.FindById(id);
            var debt = DebtMapper.Map(debtDto);

            // обновляем
            debt.Update(description, amount, creditorName);

            // приводим бизнес-объект в объект хранилища данных и сохраняем
            debtDto = DebtMapper.Map(debt);
            _debtDAO.Update(debtDto);
        }

        public void Return(Guid id)
        {
            // извлекаем объект хранилища и приводим его к бизнес-объекту
            var debtDto = _debtDAO.FindById(id);
            var debt = DebtMapper.Map(debtDto);

            // Бизнес условие - возвращенный долг нельзя вернуть повторно
            if (debt.Status == DebtStatus.Returned)
            {
                throw new InvalidOperationException("Долг уже возвращён");
            }

            // Помечаем как возвращенный
            debt.Return();

            // приводим бизнес-объект в объект хранилища данных и сохраняем
            debtDto = DebtMapper.Map(debt);
            _debtDAO.Update(debtDto);
        }
    }
}