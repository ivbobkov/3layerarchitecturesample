﻿using System;
using System.Collections.Generic;
using System.Linq;
using _3LayerArchitecture.DataAccess;
using _3LayerArchitecture.Domain.Mapping;
using _3LayerArchitecture.Domain.Models;

namespace _3LayerArchitecture.Domain.Services
{
    public class LoanService : ILoanService
    {
        private readonly ILoanDAO _loanDAO;

        public LoanService(ILoanDAO loanDao)
        {
            _loanDAO = loanDao;
        }

        public IEnumerable<Loan> GetAll()
        {
            var debtDtos = _loanDAO.GetAll();

            return debtDtos.Select(debtDto => LoanMapper.Map(debtDto));
        }

        public void Create(string description, decimal amount, string borrowerName)
        {
            // создаем бизнес-объект
            var loan = new Loan(Guid.NewGuid(), description, amount, borrowerName);

            // приводим бизнес-объект к объекту хранилища и сохраняем
            var loanDto = LoanMapper.Map(loan);
            _loanDAO.Save(loanDto);
        }

        public void Update(Guid id, string description, decimal amount, string borrowerName)
        {
            // извлекаем объект хранилища и приводим его к бизнес-объекту
            var loanDto = _loanDAO.FindById(id);
            var loan = LoanMapper.Map(loanDto);

            // обновляем
            loan.Update(description, amount, borrowerName);

            // приводим бизнес-объект в объект хранилища данных и сохраняем
            loanDto = LoanMapper.Map(loan);
            _loanDAO.Update(loanDto);
        }

        public void Close(Guid id)
        {
            // извлекаем объект хранилища и приводим его к бизнес-объекту
            var loanDto = _loanDAO.FindById(id);
            var loan = LoanMapper.Map(loanDto);

            // Бизнес условие - нельзя закрыть заём повторно
            if (loan.Status == LoanStatus.Closed)
            {
                throw new InvalidOperationException("Заём уже закрыт");
            }

            // Помечаем как закрытый
            loan.Close();

            // приводим бизнес-объект в объект хранилища данных и сохраняем
            loanDto = LoanMapper.Map(loan);
            _loanDAO.Update(loanDto);
        }
    }
}