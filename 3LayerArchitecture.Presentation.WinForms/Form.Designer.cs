﻿namespace _3LayerArchitecture.Presentation.WinForms
{
    partial class Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loans_list = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.debts_list = new System.Windows.Forms.ListView();
            this.ReturnDebt_button = new System.Windows.Forms.Button();
            this.CloseLoan_button = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.DebtDescription_input = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.DebtCreditorName_input = new System.Windows.Forms.TextBox();
            this.LoanDescription_input = new System.Windows.Forms.TextBox();
            this.LoanBorrowerName_input = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.DebtAmount_input = new System.Windows.Forms.NumericUpDown();
            this.LoanAmount_input = new System.Windows.Forms.NumericUpDown();
            this.CreateDebt_button = new System.Windows.Forms.Button();
            this.CreateLoan_button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DebtAmount_input)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LoanAmount_input)).BeginInit();
            this.SuspendLayout();
            // 
            // loans_list
            // 
            this.loans_list.FullRowSelect = true;
            this.loans_list.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.loans_list.HideSelection = false;
            this.loans_list.Location = new System.Drawing.Point(615, 31);
            this.loans_list.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.loans_list.MultiSelect = false;
            this.loans_list.Name = "loans_list";
            this.loans_list.Size = new System.Drawing.Size(548, 256);
            this.loans_list.TabIndex = 0;
            this.loans_list.UseCompatibleStateImageBehavior = false;
            this.loans_list.View = System.Windows.Forms.View.List;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Список долгов (кому должен)";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(611, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(248, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Список заемов (кому ты дал деньги)";
            // 
            // debts_list
            // 
            this.debts_list.FullRowSelect = true;
            this.debts_list.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.debts_list.HideSelection = false;
            this.debts_list.Location = new System.Drawing.Point(20, 31);
            this.debts_list.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.debts_list.MultiSelect = false;
            this.debts_list.Name = "debts_list";
            this.debts_list.Size = new System.Drawing.Size(548, 256);
            this.debts_list.TabIndex = 3;
            this.debts_list.UseCompatibleStateImageBehavior = false;
            this.debts_list.View = System.Windows.Forms.View.List;
            this.debts_list.SelectedIndexChanged += new System.EventHandler(this.debts_list_SelectedIndexChanged);
            // 
            // ReturnDebt_button
            // 
            this.ReturnDebt_button.Location = new System.Drawing.Point(20, 295);
            this.ReturnDebt_button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ReturnDebt_button.Name = "ReturnDebt_button";
            this.ReturnDebt_button.Size = new System.Drawing.Size(549, 28);
            this.ReturnDebt_button.TabIndex = 4;
            this.ReturnDebt_button.Text = "Вернуть долг";
            this.ReturnDebt_button.UseVisualStyleBackColor = true;
            this.ReturnDebt_button.Click += new System.EventHandler(this.ReturnDebt_button_Click);
            // 
            // CloseLoan_button
            // 
            this.CloseLoan_button.Location = new System.Drawing.Point(615, 295);
            this.CloseLoan_button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CloseLoan_button.Name = "CloseLoan_button";
            this.CloseLoan_button.Size = new System.Drawing.Size(549, 28);
            this.CloseLoan_button.TabIndex = 5;
            this.CloseLoan_button.Text = "Закрыть заем";
            this.CloseLoan_button.UseVisualStyleBackColor = true;
            this.CloseLoan_button.Click += new System.EventHandler(this.CloseLoan_button_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(611, 340);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Добавление заема";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 340);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Добавление долга";
            // 
            // DebtDescription_input
            // 
            this.DebtDescription_input.Location = new System.Drawing.Point(211, 370);
            this.DebtDescription_input.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DebtDescription_input.Name = "DebtDescription_input";
            this.DebtDescription_input.Size = new System.Drawing.Size(357, 22);
            this.DebtDescription_input.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 374);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Описание";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 409);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "Сумма";
            // 
            // DebtCreditorName_input
            // 
            this.DebtCreditorName_input.Location = new System.Drawing.Point(211, 437);
            this.DebtCreditorName_input.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DebtCreditorName_input.Name = "DebtCreditorName_input";
            this.DebtCreditorName_input.Size = new System.Drawing.Size(357, 22);
            this.DebtCreditorName_input.TabIndex = 14;
            // 
            // LoanDescription_input
            // 
            this.LoanDescription_input.Location = new System.Drawing.Point(805, 370);
            this.LoanDescription_input.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LoanDescription_input.Name = "LoanDescription_input";
            this.LoanDescription_input.Size = new System.Drawing.Size(357, 22);
            this.LoanDescription_input.TabIndex = 15;
            // 
            // LoanBorrowerName_input
            // 
            this.LoanBorrowerName_input.Location = new System.Drawing.Point(805, 437);
            this.LoanBorrowerName_input.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LoanBorrowerName_input.Name = "LoanBorrowerName_input";
            this.LoanBorrowerName_input.Size = new System.Drawing.Size(357, 22);
            this.LoanBorrowerName_input.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 441);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "Имя кредитора";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(611, 409);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "Сумма";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(611, 441);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 17);
            this.label9.TabIndex = 19;
            this.label9.Text = "Имя заемщика";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(611, 374);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 17);
            this.label10.TabIndex = 20;
            this.label10.Text = "Описание";
            // 
            // DebtAmount_input
            // 
            this.DebtAmount_input.Location = new System.Drawing.Point(211, 406);
            this.DebtAmount_input.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DebtAmount_input.Name = "DebtAmount_input";
            this.DebtAmount_input.Size = new System.Drawing.Size(359, 22);
            this.DebtAmount_input.TabIndex = 21;
            this.DebtAmount_input.ValueChanged += new System.EventHandler(this.NumericUpDown1_ValueChanged);
            // 
            // LoanAmount_input
            // 
            this.LoanAmount_input.Location = new System.Drawing.Point(805, 406);
            this.LoanAmount_input.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LoanAmount_input.Name = "LoanAmount_input";
            this.LoanAmount_input.Size = new System.Drawing.Size(359, 22);
            this.LoanAmount_input.TabIndex = 22;
            // 
            // CreateDebt_button
            // 
            this.CreateDebt_button.Location = new System.Drawing.Point(20, 480);
            this.CreateDebt_button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CreateDebt_button.Name = "CreateDebt_button";
            this.CreateDebt_button.Size = new System.Drawing.Size(549, 28);
            this.CreateDebt_button.TabIndex = 23;
            this.CreateDebt_button.Text = "Добавить долг";
            this.CreateDebt_button.UseVisualStyleBackColor = true;
            this.CreateDebt_button.Click += new System.EventHandler(this.CreateDebt_button_Click);
            // 
            // CreateLoan_button
            // 
            this.CreateLoan_button.Location = new System.Drawing.Point(615, 480);
            this.CreateLoan_button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CreateLoan_button.Name = "CreateLoan_button";
            this.CreateLoan_button.Size = new System.Drawing.Size(549, 28);
            this.CreateLoan_button.TabIndex = 24;
            this.CreateLoan_button.Text = "Добавить заем";
            this.CreateLoan_button.UseVisualStyleBackColor = true;
            this.CreateLoan_button.Click += new System.EventHandler(this.CreateLoan_button_Click);
            // 
            // Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1185, 533);
            this.Controls.Add(this.CreateLoan_button);
            this.Controls.Add(this.CreateDebt_button);
            this.Controls.Add(this.LoanAmount_input);
            this.Controls.Add(this.DebtAmount_input);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.LoanBorrowerName_input);
            this.Controls.Add(this.LoanDescription_input);
            this.Controls.Add(this.DebtCreditorName_input);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.DebtDescription_input);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CloseLoan_button);
            this.Controls.Add(this.ReturnDebt_button);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.debts_list);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.loans_list);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form";
            this.Text = "Управление финансами";
            this.Load += new System.EventHandler(this.Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DebtAmount_input)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LoanAmount_input)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView loans_list;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView debts_list;
        private System.Windows.Forms.Button ReturnDebt_button;
        private System.Windows.Forms.Button CloseLoan_button;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox DebtDescription_input;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox DebtCreditorName_input;
        private System.Windows.Forms.TextBox LoanDescription_input;
        private System.Windows.Forms.TextBox LoanBorrowerName_input;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown DebtAmount_input;
        private System.Windows.Forms.NumericUpDown LoanAmount_input;
        private System.Windows.Forms.Button CreateDebt_button;
        private System.Windows.Forms.Button CreateLoan_button;
    }
}

