﻿using System;
using _3LayerArchitecture.Domain.Models;

namespace _3LayerArchitecture.Presentation.WinForms.ViewModels
{
    public class LoanViewModel
    {
        public LoanViewModel(Loan loan)
        {
            Id = loan.Id.ToString();
            Text = $"Сумма: {loan.Amount} Кто: {loan.BorrowerName} Статус: {loan.Status}";
        }

        public string Id { get; }
        public string Text { get; }
    }
}