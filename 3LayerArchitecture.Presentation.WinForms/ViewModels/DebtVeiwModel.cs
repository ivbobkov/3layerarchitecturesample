﻿using System;
using _3LayerArchitecture.Domain.Models;

namespace _3LayerArchitecture.Presentation.WinForms.ViewModels
{
    public class DebtVeiwModel
    {
        public DebtVeiwModel(Debt debt)
        {
            Id = debt.Id.ToString();
            Text = $"Сумма: {debt.Amount} Кому: {debt.CreditorName} Статус: {debt.Status}";
        }

        public string Id { get; }
        public string Text { get; }
    }
}