﻿using System;
using System.Linq;
using System.Windows.Forms;
using _3LayerArchitecture.Domain.Services;
using _3LayerArchitecture.Presentation.WinForms.ViewModels;

namespace _3LayerArchitecture.Presentation.WinForms
{
    public partial class Form : System.Windows.Forms.Form
    {
        private readonly ILoanService _loanService;
        private readonly IDebtService _debtService;

        public Form(ILoanService loanService, IDebtService debtService)
        {
            _loanService = loanService;
            _debtService = debtService;
            InitializeComponent();
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void NumericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void CreateDebt_button_Click(object sender, EventArgs e)
        {
            try
            {
                _debtService.Create(DebtDescription_input.Text, DebtAmount_input.Value, DebtCreditorName_input.Text);
                RefreshDebtsList();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CreateLoan_button_Click(object sender, EventArgs e)
        {
            try
            {
                _loanService.Create(LoanDescription_input.Text, LoanAmount_input.Value, LoanBorrowerName_input.Text);
                RefreshLoansList();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void RefreshLoansList()
        {
            loans_list.Clear();

            var loans = _loanService.GetAll().Select(loan => new LoanViewModel(loan)).ToList();

            foreach (var loanViewModel in loans)
            {
                loans_list.Items.Add(loanViewModel.Id, loanViewModel.Text, 0);
            }
        }

        private void RefreshDebtsList()
        {
            debts_list.Clear();

            var debts = _debtService.GetAll().Select(debt => new DebtVeiwModel(debt)).ToList();

            foreach (var debtViewModel in debts)
            {
                debts_list.Items.Add(debtViewModel.Id, debtViewModel.Text, -1);
            }
        }

        private void ReturnDebt_button_Click(object sender, EventArgs e)
        {
            try
            {
                if (debts_list.SelectedItems.Count == 0)
                {
                    return;
                }

                var debtId = Guid.Parse(debts_list.SelectedItems[0].Name);
                _debtService.Return(debtId);
                RefreshDebtsList();
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CloseLoan_button_Click(object sender, EventArgs e)
        {
            try
            {
                if (loans_list.SelectedItems.Count == 0)
                {
                    return;
                }

                var loanId = Guid.Parse(loans_list.SelectedItems[0].Name);
                _loanService.Close(loanId);
                RefreshLoansList();
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Form_Load(object sender, EventArgs e)
        {
            RefreshLoansList();
            RefreshDebtsList();
        }

        private void debts_list_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
    }
}
