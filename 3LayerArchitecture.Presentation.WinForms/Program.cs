﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using _3LayerArchitecture.Configuration;

namespace _3LayerArchitecture.Presentation.WinForms
{
    static class Program
    {
        // можно хоть брать из параметров запуска программы, по идее просто пробросить в метод Main() вот это - string[] args и тянуть оттуда.
        private const string PathToFile = @"D:\Projects\Mentor\db.json";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var loanService = ServiceFactory.CreateLoanService(FileSystemDAOFactory.CreateLoanDAO(PathToFile));
            var debtService = ServiceFactory.CreateDebtService(FileSystemDAOFactory.CreateDebtDAO(PathToFile));
            Application.Run(new Form(loanService, debtService));
        }
    }
}
